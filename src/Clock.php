<?php

namespace Day4\Clock;

use Laravel\Nova\Fields\Field;

class Clock extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'clock';
}
