Nova.booting((Vue, router, store) => {
  Vue.component('index-clock', require('./components/IndexField'))
  Vue.component('detail-clock', require('./components/DetailField'))
  Vue.component('form-clock', require('./components/FormField'))
})
